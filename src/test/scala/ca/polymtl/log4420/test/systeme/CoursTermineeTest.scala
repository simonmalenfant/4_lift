package ca.polymtl.log4420
package test
package systeme

import org.scalatest._
import matchers.ShouldMatchers
import selenium.WebBrowser

@EquipeDeDeux( pointsDeux = 10, pointsQuatre = 5 )
class CoursTermineeTest
 extends JettySpec
 with ShouldMatchers
 with WebBrowser
 with TestCheminementMock
{
  describe( "[E] Annoter les cours terminées" )
  {
  }
}

