package ca.polymtl.log4420
package test
package unitaire

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FunSpec
import net.liftweb.http.S

@EquipeDeDeux( pointsDeux = 10, pointsQuatre = 5 )
class CheminementTestLift
  extends FunSpec
  with ShouldMatchers
  with TestLiftSession
  with TestCheminementMock
{
  /*
  Ref:
  http://www.scalatest.org/user_guide/using_selenium
  http://cookbook.liftweb.net/Testing+and+debugging+CSS+selectors.html
  https://www.assembla.com/spaces/liftweb/wiki/Testing_Lift_Applications
  */
  describe( "[B] Transformer le cheminement en html" )
  {
    it( "should" ) ( pending )
  }
}
