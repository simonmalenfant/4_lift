package ca.polymtl.log4420.test.unitaire

import net.liftweb._
import util.StringHelpers
import common.Empty
import http.{S, LiftSession}
import org.scalatest.Suite

trait TestLiftSession extends Suite
{ this: Suite =>

  // Lift snippets are stafull
  // Execute thoses tests with state
  private val newSession : LiftSession = new LiftSession("", StringHelpers.randomString(20), Empty)

  abstract override def withFixture( test: NoArgTest )
  {
    S.initIfUninitted( newSession )
    {
      super.withFixture( test )
    }
  }
}
