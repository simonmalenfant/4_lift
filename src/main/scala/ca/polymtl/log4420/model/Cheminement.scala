package ca.polymtl.log4420
package model

import net.liftweb.util.BasicTypesHelpers.asInt
import lib.CheminementInjector

// extractor patern: http://www.artima.com/pins1ed/extractors.html
object Cheminements
{
  def unapply( params: List[String] ): Option[Cheminement] =
  {
    params match
    {
      case comite :: titreChem :: annee :: Nil =>
      {
          for{ a <- asInt( annee )
              db <- CheminementInjector.DB.make
              cheminement <- db.find( ComiteProgramme( comite ), titreChem, Annee( a ) )
          } yield cheminement
      }
      case _ => None
    }
  }

  def path( cheminement: Cheminement ): List[String] =
  {
    ( cheminement.owner, cheminement.titre, cheminement.sessions ) match
    {
      case ( ComiteProgramme( comite ), titre, first :: others ) =>
      {
        comite :: titre :: first.annee.annee.toString :: Nil
      }
      case _ => {
        println( "bug" )
        Nil
      }
    }
  }


  def unapply( id: String ): Option[Cheminement] =
  {
    for {
      i <- asInt( id )
      db <- CheminementInjector.DB.make
      cheminement <- db.get( i )
    } yield cheminement
  }
}

case class Cheminement( 
	var titre: String,
	owner: Usager,
	var sessions: List[Session],
  var coursTermine: Set[Cours]
)

case class Session( 
	periode: Periode,
	annee: Annee,
	var cours: List[Cours]
)
{
  def totalCredits = cours.map( _.credit ).sum
}

case class Annee( annee: Int )
{
  override def toString = annee.toString
}

sealed trait Usager
case class Etudiant( ) extends Usager
case class ComiteProgramme( genie: String ) extends Usager