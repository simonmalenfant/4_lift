package ca.polymtl.log4420.lib

import net.liftweb._
import util.SimpleInjector

import ca.polymtl.log4420.fixture.ProgrammeDB

// injection de dépendance
// en mode test on utilise un trait
// pour isoler l'état entre chaque test

object CheminementInjector extends SimpleInjector
{
  val DB = new Inject[ProgrammeDB]( injectCheminement _ ) {}

  private lazy val db = new ProgrammeDB()
  private def injectCheminement = db
}


