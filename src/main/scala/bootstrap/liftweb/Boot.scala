package bootstrap.liftweb

import net.liftweb._
import http._
import sitemap._

import ca.polymtl.log4420.snippet.Cheminement

class Boot 
{
	def boot()
	{
    LiftRules.addToPackages( "ca.polymtl.log4420" )

    val entries = List(
        Menu.i("Home") / "index",
        Menu.i("Équipe") / "Equipe",
        Menu.i("Designer Friendly") / "DesignerFriendly",
        Menu( Cheminement.comite ),                         // Cheminement / Logiciel / Multimedia / 2009
        Menu( Cheminement.cheminement )                     // Cheminement / 1875480024
    )

    LiftRules.setSiteMap( SiteMap( entries : _* ) )

		LiftRules.early.append( _.setCharacterEncoding("UTF-8") )

		LiftRules.htmlProperties.default.set( (r: Req) =>
			new Html5Properties(r.userAgent)
		)
	}
}
